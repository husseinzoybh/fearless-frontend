function createCard(name, description, pictureUrl, starts, ends) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <p class="card-footer">${starts} - ${ends}</p>
        
      </div>

        </div>
      </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            const row = document.querySelector('.row');
            row.innerHTML += alert();

        } else {
            const data = await response.json();
            let i = 1

            for (let conference of data.conferences) {
                if (i > 3) {
                    i = 1
                }
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const startDate = details.conference.starts;
                    const endDate = details.conference.ends;
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const column = document.querySelector(`#col${i}`);
                    const starts1 = Date(details.conference.starts);
                    const starts = starts1.slice(0, 15);
                    const ends1 = Date(details.conference.ends);
                    const ends = ends1.slice(0, 15);
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    column.innerHTML += html;
                    const row = document.querySelector('.row');
                    i++
                }
            }

        }
    } catch (e) {
        const row = document.querySelector('.row');
        row.innerHTML += alert();

    }

})