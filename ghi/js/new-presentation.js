window.addEventListener('DOMContentLoaded', async () => {
    const presentationURL = 'http://localhost:8000/api/conferences/';
    const response = await fetch(presentationURL);
    if (response.ok) {
        const data = await response.json();
        const conferences = data.conferences;
        let selectTag = document.getElementById('conference');
        for (let conference of conferences) {
            let option = document.createElement("option");
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
    }
    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const submission = Object.fromEntries(formData);
        const presentationUrl = `http://localhost:8000${submission.conference}presentations/`;
        delete submission.conference;
        const json = JSON.stringify(submission);
        console.log(json);
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
        }
    });
});
